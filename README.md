# briskaR: a set of tools for spatio-temporal Biological Risk Assessment

__briskaR: Biological Risk Assessment in R__

A spatio-temporal exposure-hazard model for assessing biological risk and impact.
The model is based on stochastic geometry for describing the landscape and the exposed individuals, a dispersal kernel for the dissemination of contaminants and an ecotoxicological equation.
A model coupling polygon and point processes for assessing risk due to contaminant sources and their impact on exposed individuals.

## References

> Leclerc, M., Walker, E., Messéan, A., & Soubeyrand, S. (2018). Spatial exposure-hazard and landscape models for assessing the impact of GM crops on non-target organisms. Science of the total environment, 624, 470-479.  [doi:10.1016/j.scitotenv.2017.11.329](https://doi.org/10.1016/j.scitotenv.2017.11.329)

> Walker, E., Leclerc, M., Rey, J. F., Beaudouin, R., Soubeyrand, S., & Messéan, A. (2019). A Spatio‐Temporal Exposure‐Hazard Model for Assessing Biological Risk and Impact. Risk Analysis, 39(1), 54-70. [doi:10.1111/risa.12941](https://doi.org/10.1111/risa.12941)

# `briskaR`: R package

## Build status for development version

[![pipeline status](https://gitlab.paca.inra.fr/biosp/briskaR/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/biosp/briskaR/commits/master)

## Install

* From cran [briskaR](https://CRAN.R-project.org/package=briskaR)
* From the artifacts of _tagged_ versions
* From source as below

### Clone project

```bash
git clone git@gitlab.paca.inra.fr:biosp/briskaR.git
```

### Generate Rd file

```r
roxygen2::roxygenize('briskaR', roclets=c('rd', 'namespace'))
```

To generate the reference manual:

```r
R CMD Rd2pdf .
```

or from the parent folder

```r
R CMD Rd2pdf briskaR/
```

### Generate package

```bash
R CMD BUILD briskaR --resave-data
```


### Install package for Debug

```bash
R CMD INSTALL --no-multiarch --with-keep.source briskaR_x.x.tar.gz
```


### Test pacakge

Package unit test is done with `testthat` package. The integration of C++ test unit requires the add of a structure done with `testthat::use_catch()` (see `?use_catch` for details)

### Check package

```bash
R CMD check --as-cran briskaR_x.x.x.tar.gz
```

## DEMOS

```r
demo.pollen.run()
```

# `briskaRinference`: Bayesian inference


The folder `briskaRinference` provides a set of fucntions for parameter inference using the Bayesian language [stan](https://mc-stan.org/).

## Compilation

- `cd` to source directory
- Clean and rebuild: `R CMD INSTALL --preclean --no-multiarch --with-keep.source briskaRinference`
- Build from sources (creates vignette and archive)  `R CMD build .`
- Build and install `R CMD INSTALL --build .`
- Build and install building vignettes with R interpreter:
  `devtools::install(build_vignettes = TRUE)`
- Check the package
  `R CMD check --as-cran gutsRstan_X.X.X.tar.gz`
- Update package description/NAMESPACE
  - under the R interpreter: `roxygen2::roxygenise(".")`
- Generate documentation
  - reference manual: `R CMD Rd2pdf --output=documentation .`
  - re-build reference manual: `rm documentation | R CMD Rd2pdf --output=documentation .`
  - vignettes (using the R interpreter):
    `devtools::document(roclets=c('rd', 'collate', 'namespace', 'vignette'))`
- Run unit tests
  - under the R interpreter: `devtools::test()`

## Install `briskaRinference`

For further information about the installation, you can have a look at the installation of the `rstan` package:
- [github rstan wiki: installing RStan on Windows](https://github.com/stan-dev/rstan/wiki/Installing-RStan-on-Windows)


# `briskaRjupyter`: Jupyter notebook

The folder `briskaRjupyter` provides [jupyter](https://jupyter.org/) services for interactive computing. Using interactive comuting with jupyter aims at improving traceability of model development through a continuous interaction with experts.

## Install jupyter notebook & lab

To install jupyter on your device see: [jupyter install](https://jupyter.org/install)

For instance, you may install Python and Jupyter using the [Anaconda Distribution](https://www.anaconda.com/download/), which includes Python, the Jupyter Notebook and Jupyter Lab, and other commonly used packages for scientific computing and data science.

- [linux](https://conda.io/docs/user-guide/install/linux.html)
- [windows](https://conda.io/docs/user-guide/install/windows.html)
- [macOS](https://conda.io/docs/user-guide/install/macos.html)

To execute the script of tutorial for package `briskaR`, the [kernel for R](https://irkernel.github.io/) is required (you also need [R](https://www.r-project.org/) of course).

To [install IRkernel](https://irkernel.github.io/installation/) on linux:

```r
# install library IRkernel
install.packages('IRkernel')
# make the kernel available to Jupyter
IRkernel::installspec()
```


# AUTHORS

Virgile Baudrot
Emily Walker
Jean-François Rey
Melen Leclerc
Samuel Soubeyrand
